import { useState, useEffect } from 'react'
import firebaseApp from '../config/firebaseConfig'
import { Modal, Form, Button, Row, Col, Spinner, Alert } from 'react-bootstrap'
import { useSignInWithEmailAndPassword } from 'react-firebase-hooks/auth'
import { getAuth, sendPasswordResetEmail } from "firebase/auth";

import { useNavigate } from 'react-router-dom'

const Login = () => {
    const auth = getAuth(firebaseApp)
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
   
    const errorDict = {
        'auth/wrong-password' : 'Wrong password!',
        'auth/internal-error' : 'Password required!',
        'auth/invalid-email' : 'Email required!'
    }
    const [email, setEmail] = useState('')
    const navigate = useNavigate()
    const [credentials, setCredentials] = useState({
            email: '',
            password: ''
            })
        
    const [
            signInWithEmailAndPassword,
            user,
            loading,
            error,
            ] = useSignInWithEmailAndPassword(auth);
        
    useEffect(() => {
            if(user !== undefined)
                    navigate('/play', { replace: true });
            }, 
            [user, navigate])
        
    const loginHandler = () => signInWithEmailAndPassword(credentials.email, credentials.password)
   
    const triggerResetEmail = async () => {         
        console.log(email);         
        await sendPasswordResetEmail(auth, email )
        alert("Password Reset Email Sent")
        // console.log("Password reset email sent");     
    }  

return (
    <div>
        <Row>
            <Col xs={12} md={{span: 5, offset: 6}} className="bg-info p-4 rounded mt-5">
                <h3 className='text-dark mb-3'>Login</h3>
                <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" 
                        onKeyDown={(e) => e.key === 'Enter' && loginHandler()} onChange={(e) => setCredentials({...credentials, email: e.target.value})} value={credentials.email} 
                        />
                    </Form.Group>

                    <Form.Group className="mb-4" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" 
                            onKeyDown={(e) => e.key === 'Enter' && loginHandler()} 
                            onChange={(e) => setCredentials({...credentials, password: e.target.value})} value={credentials.password} 
                        />
                        <Button variant="outline-primary mt-2 mb-3" onClick={handleShow}>Forgot Password ?</Button>
                        <Modal show={show} onHide={handleClose} centered>
                            <Modal.Header closeButton>
                                <Modal.Title>Reset Password</Modal.Title>
                            </Modal.Header>
                            <Modal.Body> 
                                <p>Enter Your Registered Email :</p>
                                <Form.Text className="text-white" show={show} onHide={handleClose}>
                                <input  className="resetEmailInput" placeholder="Email" type="email" value={email} 
                                onChange={e => setEmail(e.target.value)} required /> 
                                </Form.Text>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button disabled={loading} onClick={triggerResetEmail}>
                                {loading ? 'loading...' : 'Send Link to Email'}
                                </Button>
                            </Modal.Footer>
                        </Modal>
                    </Form.Group>
                    
                    <Button 
                    disabled={loading} variant="primary" type="submit" onClick={loginHandler}>
                        {loading && <Spinner
                            as="span"
                            animation="grow"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                        />}
                        {loading ? 'loading...' : 'Submit'}
                    </Button>
                </Form>
            </Col>
            {error && 
            <Alert variant="danger" className='g-2'>
                {error && errorDict[error.code]}
            </Alert>}
        </Row>
    </div>
    )
}

export default Login