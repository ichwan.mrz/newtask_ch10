import { useState, useEffect } from 'react'
import { Form, Button, Row, Col, Spinner, Alert } from 'react-bootstrap'
import { useCreateUserWithEmailAndPassword } from 'react-firebase-hooks/auth'
import { getAuth } from 'firebase/auth'
import firebaseApp from '../config/firebaseConfig'
import { useNavigate } from 'react-router-dom'
import { getFirestore, collection, setDoc, doc } from 'firebase/firestore'
import { firebaseAuthError } from '../constants/errors'



const Register = () => {
    const auth = getAuth(firebaseApp)
    const navigate = useNavigate()
    const [credentials, setCredentials] = useState({
        email: '',
        password: ''
    })

  
    const [profile, setProfile] = useState({
        username: "",
        email: '',
        password:"",
        rank:0,
        point:0,
        region: ''
    })

    const [
        createUserWithEmailAndPassword,
        user, loadingRegister, errorRegister] = useCreateUserWithEmailAndPassword(auth)

    useEffect(() => {
        console.log("triggered")
        const createProfile = async () => {
            const db       = getFirestore(firebaseApp)
            const userRef  = collection(db, 'score')
            try {
                await setDoc(doc(userRef),{...profile, uid: user.user.uid})
            } catch (error) {
                console.log(error.message)
            }
        }

        if(user !== undefined && errorRegister === undefined)
            createProfile().then(() => {
            navigate('/play', { replace: true });
        })
            
    }, [user, navigate, profile, errorRegister])
    
  return (
    <div>
        <Row>
        {errorRegister && <Col xs={12} md={{span: 6, offset: 3}} className="g-0">
                <Alert variant="danger">{errorRegister.code in firebaseAuthError ? firebaseAuthError[errorRegister.code] : 'Oops, something went wrong in the server room!'}</Alert>
            </Col>}
            <Col xs={12} md={{span: 4, offset: 1}} className="bg-success p-3 rounded mt-0">
            <h4 className='text-light mb-2'>Register</h4>
                <Form>
                <Form.Group className="mb-1">
                        <Form.Label>Username</Form.Label>
                        <Form.Control type="text" placeholder="Enter your username" 
                        onChange={(e) => setProfile({...profile, username: e.target.value})} value={profile.username}
                        />
                    </Form.Group>

                    <Form.Group className="mb-1">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter your email" 
                        onChange={(e) => setCredentials({...credentials, email: e.target.value})}  value={credentials.email}
                        />
                        <Form.Control className='mt-1' type="email" placeholder="Confirm your email" 
                        onChange={(e) => setProfile({...profile, email: e.target.value})} value={profile.email}
                        />
                    </Form.Group>

                    <Form.Group className="mb-1">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Enter your password" 
                        onChange={(e) => setCredentials({...credentials, password: e.target.value})} value={credentials.password}
                        />
                        <Form.Control className='mt-1' type="password" placeholder="Confirm your password" 
                        onChange={(e) => setProfile({...profile, password: e.target.value})} value={profile.password}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Region</Form.Label>
                        <Form.Control type="text" placeholder="Enter your country" 
                            onChange={(e) => setProfile({...profile, region: e.target.value})} value={profile.region}
                        />
                    </Form.Group>

                    <Button variant="primary" disabled={loadingRegister} type="submit" onClick={() => createUserWithEmailAndPassword(credentials.email, credentials.password)}>
                        {loadingRegister && <Spinner
                            as="span"
                            animation="grow"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                        />}
                        {loadingRegister ? 'loading...' : 'Submit'}
                    </Button>
                </Form>
            </Col>
            
        </Row>
    </div>
  )
}

export default Register