import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { Link, Outlet} from "react-router-dom";
import { Nav, Navbar, Container} from 'react-bootstrap';
import { useAuthState } from 'react-firebase-hooks/auth';
import { getAuth, signOut } from 'firebase/auth';
import firebaseApp from './config/firebaseConfig';

function App() {
  const auth = getAuth(firebaseApp)
  const [user, ] = useAuthState(auth)

  return (
    <div>
      <Navbar expand="lg" className='mb-4' variant='dark' bg='dark'>
        <Container>
          <Navbar.Brand as={Link} to="/">GameHub</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
              {!user && <Nav.Link as={Link} to="/register">Register</Nav.Link>}
              {!user && <Nav.Link as={Link} to="/login">Login</Nav.Link>}
              {user && 
               (
              <div className='d-flex align-items-center'>
              <p className='text-white mb-0'>Hai, {user.displayName || user.email.split('@')[0]} !</p>
              <Nav.Link onClick={() => signOut(auth)}>Logout</Nav.Link>
              </div>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <div className="container pb-3">
      <Outlet />
      </div>
    </div>
  );
}

export default App;
