import React, { useEffect } from 'react'
// import firebase from 'firebase';
import firebaseApp from "../config/firebaseConfig";
import { getAuth } from "firebase/auth";
import { useAuthState } from "react-firebase-hooks/auth";
import { getFirestore, collection, doc, updateDoc, 
// setDoc, where, orderBy, query, limit, onSnapshot 
} from 'firebase/firestore';
import { useCollection } from 'react-firebase-hooks/firestore';
import { Table, Dropdown, DropdownButton, Button, ButtonGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const TopScore =  () => {
  const auth = getAuth(firebaseApp);
  const [user] = useAuthState(auth);
  const db = getFirestore(firebaseApp);

  const [value] = useCollection(
      collection(getFirestore(firebaseApp), 'score'),
      {
      snapshotListenOptions: { includeMetadataChanges: true },
      }
  ); 

  useEffect(() => {
      console.log(value);
  }, [value])

    // Rank and Sort not finish yet to set...maybe next time :)
  const checkPoints = async () => {
    const scoreRef = doc(db, "score", user.uid);
    await updateDoc(scoreRef, {
    point: 15,
    });
  }
  
  return (
    <div>
      <div className='container text-center bg-info mb-5'>
        <h1>
          TOP SCORER 
        </h1>
      </div>
      <div className='d-flex col '>
        <Button as={Link} to="/play" className='col-md-2 align-items-start flex-column bg-warning text-dark fw-bold border'>
              Play Again
        </Button>
        <ButtonGroup className='col d-flex align-items-end flex-column'>
            <DropdownButton as={ButtonGroup} title="Sort By" id="bg-nested-dropdown" >
              <Dropdown.Item eventKey="1">Name</Dropdown.Item>
              <Dropdown.Item onClick={checkPoints} eventKey="2">Points</Dropdown.Item>
              <Dropdown.Item eventKey="2">Country</Dropdown.Item>
            </DropdownButton>
        </ButtonGroup>
      </div>
      <Table striped bordered hover variant="dark"  className='mt-3'>
        <thead className='text-center'>
          <tr>
            <th scope="col">Rank</th>
            <th scope="col">Username</th>
            {/* <th scope="col">Email</th> */}
            <th scope="col">Points</th>
            <th scope="col">Region</th>
            </tr>
        </thead>
        <tbody> 
          {value && value.docs.map((doc) => (
          <tr key={doc.id}>
            <td className='col-1 text-center'>{doc.data().rank}</td>
            <td className='col-4 text-left fw-bold'>{doc.data().username}</td>
            {/* <td className='col-3 text-center'>{doc.data().email}</td> */}
            <td className='col-1 text-center'>{doc.data().point}</td>
            <td className='col-3 text-center'>{doc.data().region}</td>
          </tr>
          ))}
        </tbody>
      </Table>
    </div>
  )
}

export default TopScore