import React from 'react'
import { Button } from 'react-bootstrap'
import { Link} from 'react-router-dom';

const Home = () => {
  return (
    <div className='container text-center text-light mt-5'>
    <Button as={Link} to="/play" type='button' className='btn-info btn-lg fw-bold p-3 mt-5 mb-5 text-dark'>CLICK HERE TO PLAY</Button>
    <h1 className='fw-bold'>THE TRADITIONAL GAME</h1>
    </div>
  )
}

export default Home