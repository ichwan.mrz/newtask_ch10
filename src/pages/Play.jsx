import { motion } from "framer-motion";
import confetti from 'canvas-confetti';
import React, { useEffect, useState } from 'react'
import { getAuth } from "firebase/auth";
import firebaseApp from "../config/firebaseConfig";
import { useAuthState } from "react-firebase-hooks/auth";
import { getFirestore, updateDoc, doc } from "firebase/firestore";
import { Button, Image, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import batu from "../images/batu.png"
import kertas from "../images/kertas.png"
import gunting from "../images/gunting.png"
import refresh from "../images/refresh.png"
import result_cpu from "../images/win.jpg";
import result_user from "../images/lose.jpg";

const Play = () => {
const auth = getAuth(firebaseApp);
const db = getFirestore(firebaseApp);
const [user] = useAuthState(auth);
const [userChoice, setUserChoice] = useState('');
const [computerChoice, setComputerChoice] = useState('');
const [userPoints, setUserPoints] = useState(0);
const [computerPoints, setComputerPoints] = useState(0);
const [result, setResult] = useState("Let's see who wins");
const [gameOver, setGameOver] = useState(false);
const choices = ["batu", "kertas", "gunting"];


const playing = (value) => {
  // playerWin()
  setUserChoice(value);
  generateComputerChoice();
  if (value === "gunting") {
    document.getElementById("gunting").style.backgroundColor="white"
    document.getElementById("kertas").style.backgroundColor="#0b5ed7"
    document.getElementById("batu").style.backgroundColor="#0b5ed7"
    if (randomChoice === "batu") {
        comWin()
    } else if (randomChoice === "kertas") {
        playerWin()
    } else if (randomChoice === "gunting") {
        draw()
    }
  } else if (value === "batu") {
    document.getElementById("batu").style.backgroundColor="white"
    document.getElementById("kertas").style.backgroundColor="#0b5ed7"
    document.getElementById("gunting").style.backgroundColor="#0b5ed7"
    if (randomChoice === "kertas") {
        comWin()
    } else if (randomChoice === "gunting") {
        playerWin()
    } else if (randomChoice === "batu") {
        draw()
    }
  } else {
    document.getElementById("kertas").style.backgroundColor="white"
    document.getElementById("batu").style.backgroundColor="#0b5ed7"
    document.getElementById("gunting").style.backgroundColor="#0b5ed7"
    if (randomChoice === "gunting") {
        comWin()
    } else if (randomChoice === "batu") {
        playerWin()
    } else if (randomChoice === "kertas") {
        draw()
    }
  }
  console.log("user: " + value);
};


const randomChoice = choices[Math.floor(Math.random() * choices.length)];
const generateComputerChoice = () => {
  setComputerChoice(randomChoice);
  console.log("cpu: " + randomChoice);
  if (randomChoice === "gunting") {
    document.getElementById("gunting2").style.backgroundColor="white"
    document.getElementById("batu2").style.backgroundColor = "#0d6efd";
    document.getElementById("kertas2").style.backgroundColor = "#0d6efd";
  } else if (randomChoice === "batu") {
    document.getElementById("batu2").style.backgroundColor="white"
    document.getElementById("gunting2").style.backgroundColor = "#0d6efd";
    document.getElementById("kertas2").style.backgroundColor = "#0d6efd";
  } else {
    document.getElementById("kertas2").style.backgroundColor="white"
    document.getElementById("batu2").style.backgroundColor = "#0d6efd";
    document.getElementById("gunting2").style.backgroundColor = "#0d6efd";
  }
};

const points = userPoints;
const checkPoint = () => { 
  if (
      updateDoc(doc(db, "score/" , user.uid), { 
      username: "",
      email: user.email,
      password:"",
      region:"",
      rank: 0,
      point : points
  })) 
  { 
    console.log(auth.currentUser);
    console.log(user.uid);
    console.log(user.email);
    console.log(result);
    console.log(userPoints) 
    console.log(computerPoints)
    console.log(points);
    console.log("Profile updated!"); 
  }
  else {
    console.log("something wrong");
  };
}

  function draw() {
    document.getElementById("draw").style.display = "block";
    document.getElementById("versus").style.display = "none";
    document.getElementById("playerWin").style.display = "none";
    document.getElementById("comWin").style.display = "none";
  }

const playerWin = () => {
    document.getElementById("playerWin").style.display = "block";
    document.getElementById("versus").style.display = "none";
    document.getElementById("draw").style.display = "none";
    document.getElementById("comWin").style.display = "none";
}

const comWin = () => {
    document.getElementById("comWin").style.display = "block";
    document.getElementById("versus").style.display = "none";
    document.getElementById("draw").style.display = "none";
    document.getElementById("playerWin").style.display = "none";
   
}
    
const reset = () => {
    setGameOver(false);
    setUserPoints(0);
    setComputerPoints(0);
    //not set-up yet to stop confetti
    document.getElementById("batu").style.backgroundColor = "#0b5ed7"
    document.getElementById("kertas").style.backgroundColor = "#0b5ed7"
    document.getElementById("gunting").style.backgroundColor = "#0b5ed7";
    document.getElementById("batu2").style.backgroundColor = "#0d6efd";
    document.getElementById("kertas2").style.backgroundColor = "#0d6efd";
    document.getElementById("gunting2").style.backgroundColor = "#0d6efd";
    document.getElementById("versus").style.display = "block";
    document.getElementById("draw").style.display = "none";
    document.getElementById("playerWin").style.display = "none";
    document.getElementById("comWin").style.display = "none";
}

const celebrate = () => {
    let duration = 15 * 1000;
    let animationEnd = Date.now() + duration;
    let defaults = { startVelocity: 30, spread: 360, ticks: 60, zIndex: 0 };
    function randomInRange(min, max) {
      return Math.random() * (max - min) + min;
    }
    let interval = setInterval(function() {
    let timeLeft = animationEnd - Date.now();
    if (timeLeft <= 0) {
      return clearInterval(interval);
    }

    let particleCount = 50 * (timeLeft / duration);
      confetti(Object.assign({}, defaults, { particleCount, origin: { x: randomInRange(0.1, 0.3), y: Math.random() - 0.2 } }));
      confetti(Object.assign({}, defaults, { particleCount, origin: { x: randomInRange(0.7, 0.9), y: Math.random() - 0.2 } }));
    }, 250);
}

const snow = () => {
    let duration = 5 * 1000;
    let animationEnd = Date.now() + duration;
    let skew = 1;

    function randomInRange(min, max) {
      return Math.random() * (max - min) + min;
    }

    (function frame() {
      let timeLeft = animationEnd - Date.now();
      let ticks = Math.max(200, 500 * (timeLeft / duration));
      skew = Math.max(0.8, skew - 0.001);

      confetti({
        particleCount: 1,
        startVelocity: 0,
        ticks: ticks,
        origin: {
          x: Math.random(),
          y: (Math.random() * skew) - 0.2
        },
        colors: ['#ffffff'],
        shapes: ['circle'],
        gravity: randomInRange(0.4, 0.6),
        scalar: randomInRange(0.4, 1),
        drift: randomInRange(-0.4, 0.4)
      });

      if (timeLeft > 0) {
        requestAnimationFrame(frame);
      }
    }());
}



useEffect(() => {
  const pilihan = userChoice + computerChoice;
  if (userPoints <= 4 && computerPoints <= 4) {
    if (
      pilihan === "guntingkertas" ||
      pilihan === "batugunting" ||
      pilihan === "kertasbatu"
    ) {
      const updatedUserPoints = userPoints + 1;
      setUserPoints(updatedUserPoints);
    
      if (updatedUserPoints === 5) {
        setResult("You Win, Score :");
       
        const gameOff = true;
        setTimeout(() => {
          setGameOver(gameOff);
          confetti({
            particleCount: 500,
            spread: 120,
            origin: { y: 0.6 }
          });
        }, 500);
      } 
    }

    if (
      pilihan === "kertasgunting" ||
      pilihan === "guntingbatu" ||
      pilihan === "batukertas"
    ) {
      const updatedComputerPoints = computerPoints + 1;
      setComputerPoints(updatedComputerPoints);
      if (updatedComputerPoints === 5) {
        setResult("You Lose, Score :");
       
        const gameOff = true;
        setTimeout(() => {
          setGameOver(gameOff);
          snow()
        }, 500);
      }
    }
  }
},[userChoice, computerChoice]);


  return (
    <div className='container' style={{width: "88%"}}> 
        <div className="row-2 d-flex gap-5 mb-2 justify-content-center">
            <div className="celebrate">
                <Button className="bg-info rounded" onClick={celebrate} >
                  <strong>Click Here</strong><br/>To Celebrate Your Self !
                </Button>
            </div>
            <div>
              {!gameOver && (
                <div>
                  <div className="score text-center">
                    {gameOver && <p>{result}</p>}
                    <div className="hp-user">
                      <div className="text-light">Cpu
                        <progress
                          className="user-hp"
                          value={5 - computerPoints}
                          max="5"
                        ></progress>
                      </div>
                    </div>
                    <div className="hp-cpu">
                      <div className="text-light">You
                        <progress
                          className="cpu-hp"
                          value={5 - userPoints}
                          max="5"
                        ></progress>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
            <div className="topscore">
                  <Button onClick={checkPoint} as={Link} to="/topScore" className="bg-info rounded" >
                    <strong>Click Here</strong><br/>To Check The Top Scorer
                  </Button>
            </div>
        </div>
       
        <div className=' ui-game container col-3 md-3 gap-3 d-flex justify-content-center border' style={{width:'600px'}} >
            <div className='container'>
                <Row>
                    <h1 className='text-center text-light'>Player</h1>
                    <Row className='player1 m-1'>
                        <Button onClick={() => playing(choices[0])} className='mt-3 mb-3' id='batu' value={0}>
                            <Image style={{width: '70px'}} src={batu} alt='batu'/>
                        </Button> 
                        <Button onClick={() => playing(choices[1])} className='mb-3' id='kertas' value={1}>
                            <Image style={{width: '50px'}} src={kertas} alt='kertas'/>
                        </Button> 
                        <Button onClick={() => playing(choices[2])} className='mb-3' id='gunting' value={2}>
                            <Image style={{width: '70px'}} src={gunting} alt='gunting' />
                        </Button> 
                    </Row>
                </Row>
            </div>
      
            <div className='row align-items-center' >
                <div className='col px-5 py-3 mt-5 align-items-center' id='versus' >
                    <h1 className='fw-bold bg-warning border px-5 py-2 align-items-center' >VS</h1>
                </div>
            </div>

                <h1 id="draw" style={{
                    width: "25%",
                    height: "25%",
                    backgroundColor: "darkgreen",
                    fontSize: "medium",
                    fontWeight: "bold",
                    textTransform:" uppercase",
                    textAlign: "center",
                    justifyContent: "center",
                    marginTop: "150px",
                    marginLeft: "48px",
                    marginRight: "48px",
                    color: "white",
                    padding: "40px 41px",
                    transform: "rotate(-25deg)",
                    display: "none",
                    }}>Draw
                </h1>
                <h1 id="playerWin" style={{
                    width: "25%",
                    height: "25%",
                    backgroundColor: "darkgreen",
                    fontSize: "medium",
                    fontWeight: "bold",
                    textTransform:" uppercase",
                    textAlign: "center",
                    justifyContent: "center",
                    marginTop: "150px",
                    marginLeft: "48px",
                    marginRight: "48px",
                    color: "white",
                    padding: "30px 37px",
                    transform: "rotate(-25deg)",
                    display: "none",
                    }}>Player Win
                </h1>
                <h1 id="comWin" style={{
                    width: "25%",
                    height: "25%",
                    backgroundColor: "darkgreen",
                    fontSize: "medium",
                    fontWeight: "bold",
                    textTransform:" uppercase",
                    textAlign: "center",
                    justifyContent: "center",
                    marginTop: "150px",
                    marginLeft: "48px",
                    marginRight: "48px",
                    color: "white",
                    padding: "30px 22px",
                    transform: "rotate(-25deg)",
                    display: "none",
                    }}>Computer Win
                </h1>

            <div className='container'>
                <Row>
                    <h1 className='text-center text-light'>Compt</h1>
                    <Row className='player2 m-1'>
                        <Button disabled className='computer mt-3 mb-3' id='batu2' value={0}>
                            <Image style={{width: '70px'}} src={batu} alt='batu' />
                        </Button> 
                        <Button disabled className='computer mb-3'  id='kertas2' value={1}>
                            <Image style={{width: '50px'}} src={kertas} alt='kertas'/>
                        </Button> 
                        <Button disabled className='computer mb-3' id='gunting2' value={2}>
                            <Image style={{width: '70px'}} src={gunting} alt='gunting' />
                        </Button> 
                    </Row>
                </Row>
            </div>
        </div>
        <div>
            <center>
                  <Button onClick={reset} className='mt-2' type='button' id='reset'>
                      <Image style={{width: '50px'}} src={refresh} alt='reset'/>
                  </Button>
                      <h6 className='text-light'>Reset</h6>
            </center>
        </div>
     
        <div className="game">
          {gameOver && (
            <motion.div
              className="result text-center"
              animate={{ scale: 1.3 }}
              transition={{
                duration: 0.5,
              }}
            >
              {result === "You Win, Score :"}
              <motion.img
                src={result === "You Lose, Score :" ? result_user : result_cpu}
                alt=""
                style={{
                  width:"194px", 
                  marginTop: "-500px"}}
                animate={{
                  scale: [1, 1.5, 1.5, 1, 1],
                  rotate: [0, 0, 270, 270, 0],
                }}
                transition={{ duration: 1 }}
              />
              <div className="bg-secondary p-2 text-center text-light" style={{width: "100%",marginTop:"-20%"}}>
              <h3 className="result-msg">{result}</h3>
              <h4 className="result-score" >
                [ {userPoints} - {computerPoints} ]
              </h4>
              </div>
            </motion.div>
          )}
        </div>
    </div>
  )
}

export default Play